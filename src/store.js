import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

const api = axios.create({
  baseURL: 'http://localhost:8000/',
  timeout: 1000
})

export default new Vuex.Store({
  state: {
    characters: [],
    frames: []
  },
  getters: {
    // TODO: fill in
  },
  mutations: {
    'GET_CHARACTERS': (state, response) => {
      state.characters = response.data
    },
    'GET_FRAMES': (state, response) => {
      state.frames = response.data
    },
    'API_FAIL': (state, error) => {
      console.error(error)
    }
  },
  actions: {
    getCharacters (store) {
      return api.get('characters/')
        .then((response) => store.commit('GET_CHARACTERS', response))
        .catch((error) => store.commit('API_FAIL', error))
    },
    getFrames (store, characterId) {
      return api.get(`characters/${characterId}/moves`)
        .then((response) => store.commit('GET_FRAMES', response))
        .catch((error) => store.commit('API_FAIL', error))
    }
  }
})

